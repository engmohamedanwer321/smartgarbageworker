package com.smartgarbage;

import android.app.Application;
import org.reactnative.camera.RNCameraPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.facebook.react.ReactApplication;
import com.safaeean.barcodescanner.BarcodeScannerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;

import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.bridge.NavigationReactPackage;

import com.sbugert.rnadmob.RNAdMobPackage;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.safaeean.barcodescanner.BarcodeScannerPackage;
import org.wonday.pdf.RCTPdfView;

import java.util.Arrays;
import java.util.List;
import com.wix.RNCameraKit.RNCameraKitPackage;
import ui.toasty.RNToastyPackage;

public class MainApplication extends NavigationApplication {


  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList( // <== this
            new MainReactPackage(),
            new RNCameraPackage(),
            new BarcodeScannerPackage(),
            new RNCameraKitPackage(),
            new PickerPackage(),
            new VectorIconsPackage(),
            new RNAdMobPackage(),
            new SvgPackage(),
            new RNGestureHandlerPackage(),
            new LottiePackage(),
            new ReactNativeLocalizationPackage(),
            new RNFetchBlobPackage(),
            new RNToastyPackage(),
            new RCTPdfView(),
            new NavigationReactPackage(),
            new RNFusedLocationPackage(),
            new FastImageViewPackage(),
            new MapsPackage(),
            new LocationServicesDialogBoxPackage(),
            new FIRMessagingPackage()
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }
  @Override
  public String getJSMainModuleName() {
    return "index";
  }

}
