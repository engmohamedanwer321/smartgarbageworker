import {Alert} from 'react-native';
import {
    CHANGE_LANGUAGE,
} from './types';
export default function changeLanguage(change) {
    return  (dispatch) => {
        dispatch({ type: CHANGE_LANGUAGE,payload:change});  
    }
}