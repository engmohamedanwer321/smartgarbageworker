import React,{Component} from 'react';
import {NetInfo,Platform, View,TouchableOpacity,Image,Text,Keyboard,AsyncStorage,StyleSheet} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import { Button } from 'native-base';
import { RNToasty } from 'react-native-toasty';
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import SnackBar from 'react-native-snackbar-component';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import Scanbarcode from 'react-native-scan-barcode';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import strings from '../assets/strings';
import StepIndicator from 'react-native-step-indicator';
import Swiper from "react-native-swiper";
const MyButton =  withPreventDoubleClick(Button);




class AddTask extends Component{
   
    trashID;
    swiper = null;

    state = {
        currentPage: 0,
        images:["",""],
        uploadTask:false,
        noConnection:null,
        torchMode: 'off',
        cameraType: 'back',
        openScanCode: false,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };


    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

   
    onAddTask = async () => {
        this.setState({ uploadTask: true })
        var data = new FormData();
        this.state.images.filter(img => {
            data.append('confirm', {
                uri: img,
                type: 'multipart/form-data',
                name: 'TaskImages'
            })
        })
        console.log("data is")
        console.log(data)

        axios.put(`${BASE_END_POINT}trash/${this.props.trashID}/empty`, data, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${this.props.currentUser.token}`
            }
        })
        .then(response => {
            console.log(response);
            console.log('okosoksoksoksoskoskos')
            this.setState({ uploadTask: false })
            RNToasty.Success({ title: Strings.addTasktSuccess })
            AsyncStorage.removeItem('@TrachID');
            setTimeout(() => {
                this.props.navigator.pop()
            }, 1500);
        }).catch(error => {
            console.log(error);
            console.log(error.response);
            this.setState({ uploadTask: false })
            if (!error.response) {
                this.setState({ noConnection: "error" })
            }
        })            
    }    

   

    renderAddButton() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={()=>{this.onAddTask()}}
            style={{alignSelf:'center', marginTop:moderateScale(20), justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(45),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.done} color='white' />
            </MyButton>
        );
    }


   

     renderNoConnection = () => (
        <SnackBar
        visible={this.state.noConnection!=null?true:false} 
        textMessage={Strings.noConnection}
        messageColor='white'
        backgroundColor={colors.primaryColor}
        autoHidingTime={5000}
        />
     )

     pickupImageFromCamera = (imgNum) =>{
        ImagePicker.openCamera({
            width: responsiveWidth(45),
            height: responsiveHeight(35),
            cropping: true,
          }).then(image => {
            console.log(image);
            const newImages = [...this.state.images];
            newImages[imgNum] = image.path;
            this.setState({images:newImages})
          });
     }

     renderImagesPart = () => {
     const {isRTL} = this.props;
     return (
        <View style={{height:responsiveHeight(62)}}>
        <View style={{ marginTop:moderateScale(20), height:responsiveHeight(35), width:responsiveWidth(94), alignSelf:'center',flexDirection:isRTL?'row-reverse':'row'}}>
            <TouchableOpacity
             onPress={()=>{this.pickupImageFromCamera(0)}}
             style={{elevation:2, backgroundColor:'#f9f9f9', justifyContent:'center',alignItems:'center', marginVertical:moderateScale(7),flex:1}}>
                {
                    this.state.images[0]===""?
                    <View style={{alignSelf:'center'}}>
                        <Text>{strings.addImage} 1</Text>
                    </View>
                    :
                    <Image style={{flex:1,width:responsiveWidth(45)}} source={{uri:this.state.images[0]}}/>
                }
            </TouchableOpacity>

            <TouchableOpacity
            onPress={()=>{this.pickupImageFromCamera(1)}}
             style={{elevation:2, marginHorizontal:moderateScale(2), backgroundColor:'#e0e0e0', justifyContent:'center',alignItems:'center', marginVertical:moderateScale(7),flex:1}}>
                {
                    this.state.images[1]===""?
                    <View style={{alignSelf:'center'}}>
                        <Text>{strings.addImage} 2</Text>
                    </View>
                    :
                    <Image style={{flex:1,width:responsiveWidth(45)}} source={{uri:this.state.images[1]}}/>

                }
            </TouchableOpacity>
        </View>
        <TouchableOpacity
        onPress={()=>{
            const images = this.state.images;
            if(images[0]===''||images[1]===''){
                RNToasty.Warn({title: Strings.choose2Images});
                //this.setState({currentPage:2})
                //this.swiper.scrollBy(1);
            }else{
                this.setState({currentPage:2})
                this.swiper.scrollBy(1);
            }
        }}
         style={{alignSelf:isRTL?'flex-start':'flex-end',position:'absolute', bottom:0}}>
            <Text style={{marginHorizontal:moderateScale(10),fontSize:responsiveFontSize(3)}}>{Strings.next}</Text>
        </TouchableOpacity>
    </View>
     )
    }

    renderBarcodePart = () => {
        const {isRTL} = this.props;
        return(
            <View style={{ height: responsiveHeight(62) }}>
               
                {
                    this.state.openScanCode?
                    <Scanbarcode
                    onBarCodeRead={this.barcodeReceived}
                    style={{ flex: 1 }}
                    torchMode={this.state.torchMode}
                    cameraType={this.state.cameraType}
                  />
                  :
                  <View style={{ marginTop: moderateScale(20), height: responsiveHeight(38), width: responsiveWidth(60), alignSelf: 'center' }}>
                    <TouchableOpacity
                        onPress={()=>{
                           
                        this.setState({openScanCode:true})                          
                        }}
                        //onPress={()=>{this.pickupImageFromCamera(0)}}
                        style={{ elevation: 2, backgroundColor: '#f9f9f9', justifyContent: 'center', alignItems: 'center', marginVertical: moderateScale(7), flex: 1 }}
                    >
                        <Image style={{ height: responsiveHeight(25), width: responsiveWidth(45) }} source={require('../assets/imgs/bc.png')} />
                    </TouchableOpacity>
                  </View>
                }

            </View> 
        )
    }

    barcodeReceived = (e) => {
        if(e){
            console.log('Barcode: ' + e.data);
            console.log('Type: ' + e.type);
            this.setState({openScanCode:false, currentPage:1})
            this.swiper.scrollBy(1);
        }
      }
    
   
    render(){
        const {isRTL} = this.props;
        return(
            <View style={{ flex: 1 }}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.addTask} />
                <View style={{marginVertical:moderateScale(15)}}>
                    <StepIndicator
                        onPress={(position) => {
                            //this.setState({currentPage:position});
                            //Alert.alert(2+"");
                        }}
                        stepCount={3}
                        customStyles={firstIndicatorStyles}
                        currentPosition={this.state.currentPage}
                        labels={[Strings.readBarcode,Strings.pickImages,Strings.confirm]} />
                </View>

                <View style={{ width:responsiveWidth(100),height:responsiveHeight(62)}}>
                    <Swiper
                        style={{ flex: 1 }}
                        loop={false}
                        autoplay={false}
                        scrollEnabled={false}
                        showsButtons={false}
                        showsPagination={false}
                        ref={(s) => this.swiper = s}
                        index={this.state.currentPage}
                    >
        
                        {this.renderBarcodePart()}
                        {this.renderImagesPart()}
                        {this.renderAddButton()}
                       
                    </Swiper>
                </View>

                {this.state.uploadTask && <LoadingDialogOverlay title={Strings.addTaskWait} />}
                {this.renderNoConnection()}
            </View>          
        )
    }
}

const firstIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: colors.primaryColor,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.primaryColor,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: colors.primaryColor,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: colors.primaryColor,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.primaryColor,
  stepIndicatorLabelFinishedColor: 'white',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#cbc4bf',
    labelSize: 17,
    currentStepLabelColor: colors.primaryColor,
    labelFontFamily: Platform.OS == 'ios' ? 'Droid Arabic Kufi' : 'droidkufi',
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
})



export default connect(mapToStateProps)(AddTask);