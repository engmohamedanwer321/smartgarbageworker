import React,{Component} from 'react';
import {View,Image,AsyncStorage} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Button} from 'native-base';
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import AppHeader from '../common/AppHeader';
import  changeLanguage from '../actions/LanguageActions';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';



class SelectLanguage extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }
     
    render(){
        return(
            <View style={{flex:1}}>
            {!this.props.hideMenu&&<AppHeader navigator={this.props.navigator} showBurger  title={Strings.selectLanguage} /> }
                
                <Image style={{alignSelf:'center',marginVertical:hp(10), width:wp(80),height:hp(10)}} source={require('../assets/imgs/smartTrach2.jpg')} />
                <View style={{alignSelf:'center'}}>
                    <Button
                    onPress={()=>{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.hideMenu&&this.props.navigator.resetTo({
                            screen:'Login'
                        })
                    }}
                     style={{backgroundColor:colors.darkPrimaryColor,justifyContent:'center',alignItems:'center',width:wp(50),hp:hp(6.5),borderRadius:wp(10)}} >
                        <AppText fontSize={wp(5)} text='English' color='white'/>
                    </Button>

                    <Button
                     onPress={()=>{
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                        this.props.hideMenu&&this.props.navigator.resetTo({
                            screen:'Login'
                        })
                    }}
                    style={{marginVertical:hp(4), backgroundColor:colors.buttonColor,justifyContent:'center',alignItems:'center',width:wp(50),hp:hp(6.5),borderRadius:wp(10)}} >
                    <AppText fontSize={wp(5)} text='العربية' color='white'/>
                    </Button>
                </View>
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
   
})

const mapDispatchToProps = {
    changeLanguage,
}


export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);