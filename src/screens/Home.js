import React,{Component} from 'react';
import {
     View,TouchableOpacity,NetInfo,AsyncStorage,Dimensions,Platform
     ,FlatList,RefreshControl,StyleSheet,Text
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Badge } from 'native-base';
import AppHeader from '../common/AppHeader'
import Icon from 'react-native-vector-icons/FontAwesome5';
import strings from '../assets/strings';
import SnackBar from 'react-native-snackbar-component';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import ActionButton from 'react-native-action-button';
import axios from 'axios';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import ListFooter from '../components/ListFooter';
import TaskCard from '../components/TaskCard';
import {getNotifications} from '../actions/NotificationAction'; 
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';




import ReactNativeParallaxHeader from 'react-native-parallax-header';


  
  //ca-app-pub-9566226877101037~2778204178

  //ca-app-pub-9566226877101037/2119852334

class Home extends Component {
   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    list=[];
    page=1;
    state = {  
        noConnection:null,
        tasks: new DataProvider(),
        flag:0,
        refresh:false,
        loading:false,
        pages:null, 
    };

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = wp(99);
            dim.height = hp(29);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
               
               this.props.getNotifications(this.page,false,this.props.currentUser.token);
               this.getTasks()
            }else{
                this.setState({noConnection:'Strings.noConnection'})
            }
          });
      } 


    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    getTasks(page, refresh) {
    this.setState({loading:true,flag:0})
    let uri = `${BASE_END_POINT}getAllTasks?worker=${this.props.currentUser.user.id}&page=${page}&limit=10`
    if (refresh) {
        this.setState({loading: false, refresh: true})
    } else {
        this.setState({refresh:false,loading:true})
    }
    axios.get(uri,
        {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }
        )
        .then(response => {
            console.log("products under category")
            console.log(response.data)
            this.setState({
                loading:false,
                refresh:false,
                pages:response.data.pageCount,
                noConnection:null,
                tasks: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.tasks.getAllData(), ...response.data.data]),
            })
            if(page==1){
                if(response.data.data.length==0){
                    RNToasty.Info({title:strings.noProducts})
                }
            }
        }).catch(error => {
            console.log("products under category error")
            this.setState({loading:false,refresh:false})
            console.log(error.response);
            if (!error.response) {
                this.setState({errorText:strings.noConnection})
            }
        })

}
    
    
    componentDidMount(){
        AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
       setInterval(()=>{
        AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
       },1000*120)
        this.enableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getTasks()
                }
            }
          );
        
    }


    renderRow = (type, data, row) => {
        console.log('data')
        console.log(data)
        return (
            <TaskCard data={data} />
        );
       }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }
   
    renderTasksList = () => (
        <RecyclerListView
            //scrollViewProps={vi}
            layoutProvider={this.renderLayoutProvider}
            dataProvider={this.state.tasks}
            rowRenderer={this.renderRow}
            renderFooter={this.renderFooter}
            onEndReached={() => {

                if (this.page <= this.props.pages) {
                    this.page++;
                    this.getTasks(this.page, false);
                }

            }}
            refreshControl={<RefreshControl colors={["#B7ED03"]}
                refreshing={this.state.refresh}
                onRefresh={() => {
                    this.page = 1
                    this.getTasks(this.page, true);
                }
                } />}
            onEndReachedThreshold={.5}

        />

    )

    leftComponent = () => (
        <View style={{}}>
            <TouchableOpacity
            onPress={()=>{
                if(this.props.currentUser){
                    this.props.navigator.push({
                        screen:'Notifications',
                        animated:true,
                      })
                }else{
                    RNToasty.Warn({title:strings.checkUser}) 
                }
            }}
             style={{marginHorizontal:moderateScale(7)}}>
                {
                    this.props.unReaded!=0 &&
                    <Badge danger style={{elevation:2, height:20, justifyContent:'center',alignItems:'center'}}>
                        <Text style={{alignSelf:'center', fontSize:10, color:'white'}}>{this.props.unReaded}</Text>
                    </Badge>
                }
                <Icon style={{marginTop:this.props.unReaded!==0?moderateScale(-3):0}} name='bell' color='white' size={20} />
            </TouchableOpacity>
            
        </View>
    )


    render(){
        const {isRTL} = this.props;
        return(
            <View style={{ flex:1}}>
                <AppHeader title={strings.home}  showBurger leftComponent={this.leftComponent()} navigator={this.props.navigator} />            
               
                {this.state.loading?
                <View style={{flex:1}}>
                    <View style={{ flex:1}}> 
                    <LottieView
                      style={{margin:0,padding:0, width:wp(100),height:hp(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>

                    <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:wp(100),height:hp(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>

                    <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:wp(100),height:hp(33.3)}}
                        source={require('../assets/animations/smartGarbageLoading.json')}
                        autoPlay
                        loop
                        />
                    </View>
                    

                </View>
                :
                this.renderTasksList()
                }
               

                <ActionButton
                    onPress={async () => {
                        const trashID = await AsyncStorage.getItem('@TrachID');
                        if(trashID){
                            this.props.navigator.push({
                                screen: 'AddTask',
                                animated: true,
                                passProps: {trashID:trashID}
                            })
                        }else{
                            RNToasty.Info({title:strings.youNotHaveTask})
                        }
                    }}
                    active={this.state.fabActive}
                    hideShadow={true}
                    position={isRTL ? 'left' : 'right'}
                    buttonColor={colors.darkPrimaryColor}
                    renderIcon={() => <Icon color='white' size={20} name='plus' />}
                    renderIcon={() => <Icon color='white' size={20} name='plus' />}
                /> 

                <SnackBar
                    visible={this.state.noConnection!=null?true:false} 
                    textMessage={this.state.noConnection}
                    messageColor='white'
                    backgroundColor={colors.primaryColor}
                    autoHidingTime={5000}
                    />
                
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
    unReaded: state.noti.unReaded,   

})

const mapDispatchToProps = {
    getNotifications
}

export default connect(mapToStateProps,mapDispatchToProps)(Home);