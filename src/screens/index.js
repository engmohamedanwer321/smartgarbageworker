import { Navigation } from 'react-native-navigation';
import { Provider } from "react-redux";
import store from "../store";
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

import MenuContent from "../components/MenuContent";
import SplashScreen from '../screens/SplashScreen';
import About from './About';
import AddTask from  '../screens/AddTask';
import ChangePassword from './ChangePassword';
import ContactUs from './ContactUs';
import ForgetPassword from './ForgetPassword';
import Home from '../screens/Home';
import Login from '../screens/Login';
import NotificatioDetails from  '../screens/NotificatioDetails';
import Notifications from './Notifications';
import SelectLanguage from './SelectLanguage';
import TermsAndCondictions from '../screens/TermsAndCondictions';
import UpdateProfile from './UpdateProfile';

export function registerScreens() {
     Navigation.registerComponent("NotificatioDetails", () => NotificatioDetails, store, Provider);
     Navigation.registerComponent("AddTask", () => AddTask, store, Provider);
     Navigation.registerComponent("SplashScreen", () => SplashScreen, store, Provider);
     Navigation.registerComponent("About", () =>gestureHandlerRootHOC(About), store, Provider);
     Navigation.registerComponent("ContactUs", () =>gestureHandlerRootHOC(ContactUs), store, Provider);    
     Navigation.registerComponent("Login", () => gestureHandlerRootHOC(Login), store, Provider);
     Navigation.registerComponent("MenuContent", () => gestureHandlerRootHOC(MenuContent), store, Provider);
     Navigation.registerComponent("Home", () => gestureHandlerRootHOC(Home), store, Provider)
     Navigation.registerComponent("TermsAndCondictions", () => gestureHandlerRootHOC(TermsAndCondictions), store, Provider);  
     Navigation.registerComponent("SelectLanguage", () => gestureHandlerRootHOC(SelectLanguage), store, Provider);
     Navigation.registerComponent("Notifications", () => gestureHandlerRootHOC(Notifications), store, Provider);
    
     Navigation.registerComponent("ChangePassword", () => gestureHandlerRootHOC(ChangePassword), store, Provider);
     Navigation.registerComponent("UpdateProfile", () => gestureHandlerRootHOC(UpdateProfile), store, Provider);
     Navigation.registerComponent("ForgetPassword", () => gestureHandlerRootHOC(ForgetPassword), store, Provider);

    
    }

