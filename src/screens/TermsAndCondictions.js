import React,{Component} from 'react';
import {View,Text,Dimensions,StyleSheet,Alert} from 'react-native';
import { connect } from 'react-redux';
import Pdf from 'react-native-pdf';
import * as colors from '../assets/colors';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import AppHeader from '../common/AppHeader';
import AppText from '../common/AppText';
import Strings from '../assets/strings';

class TermsAndCondictions extends Component {

    state = {
        pageNm: 1,
    }
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    componentDidMount(){    
        this.disableDrawer(); 
        console.log('anwer  '+this.props.isRTL)    
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    leftComponent = () =>(
        <AppText text={this.state.pageNm} color='white' fontSize={22} />
    )

    render(){
        return(
            <View style={{flex:1, justifyContent: 'flex-start',}}>
            <AppHeader leftComponent={this.leftComponent()} navigator={this.props.navigator} title={Strings.terms} showBack/>
                <Pdf
                    onPageChanged={(page)=>{
                        this.setState({pageNm:page});
                    }}
                    enablePaging
                    fitWidth
                    source={require('../assets/promoDocumentation.pdf')}
                    style={{width:responsiveWidth(100),height:responsiveHeight(90)}}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
})

export default connect(mapToStateProps)(TermsAndCondictions);
