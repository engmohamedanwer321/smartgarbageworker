import React,{Component} from 'react';
import {
     View,Image
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import strings from '../assets/strings';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';





class About extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }




    render(){
        const {isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                
                <AppHeader title={strings.aboutUS}   showBurger navigator={this.props.navigator} />
                
                <Image style={{alignSelf:'center',marginTop:hp(10), width:wp(80),height:hp(10)}} source={require('../assets/imgs/smartTrach2.jpg')} />
                
                <View style={{alignSelf:'center',marginTop:hp(3)}}>
                    <AppText color='gray' text={strings.version} />
                </View>
               
                <View style={{marginHorizontal:wp(4), alignSelf:isRTL?'flex-end':'flex-start', marginTop:hp(17)}}>
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <AppText color={colors.buttonColor} fontWeight='500' text={strings.appName} fontSize={wp(5.5)} />
                    </View>
                        
                    <AppText color='gray'  text={strings.aboutTitle} fontSize={wp(4)} />
                </View>
                
                <View style={{backgroundColor:'#f2efef',width:wp(100),height:hp(7),position:'absolute',bottom:0,right:0,left:0,}}>
                <View style={{marginHorizontal:wp(4),  flexDirection:isRTL?'row-reverse':'row',alignItems:'center' }}>
                    <Image style={{marginHorizontal:wp(3), width:wp(11),height:hp(6)}} source={require('../assets/imgs/a.jpg')} />
                    <AppText color='black'   text={strings.thisAppDesigned} fontSize={wp(4)} />
                    <AppText color={colors.buttonColor} fontWeight='600'  text={strings.firmName} fontSize={wp(4.8)} />
                </View>
                </View>
            
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
})


export default connect(mapToStateProps)(About);