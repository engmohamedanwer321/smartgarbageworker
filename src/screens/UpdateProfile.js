import React, { Component } from 'react';
import {
  View,ImageBackground,Keyboard,ScrollView,Modal,TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Button,  Icon, Item, Picker, Label, Thumbnail } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"

import SnackBar from 'react-native-snackbar-component';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog, { SlideAnimation } from 'react-native-popup-dialog';


const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    if (!values.address) {
        errors.address = Strings.require;
    }

    
    return errors;
};
;

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class UpdateProfile extends Component {

    state = {
        userImage:null,
        showModal: false,
    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator; 
    }
    
    componentDidMount(){    
        this.disableDrawer();
    }
//

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    
 
    showImage = () => {
        const {currentUser} = this.props;
        return(
          
            <Dialog
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                width={wp(80)}
                height={hp(60)}
                visible={this.state.showModal}
                onTouchOutside={() => {
                this.setState({ showModal: false });
                }}
            >
                
                <ImageBackground  style={{flex:1,margin:wp(2)}} source={{uri:currentUser.user.img}}>

                </ImageBackground>
               
            </Dialog>
          

        )
    }

    
    
    
    
renderContent() {
    const { isRTL } = this.props;
    return (
        <View>

            <View style={{width:wp(80), flexDirection:isRTL?'row-reverse':'row'}}>
                <View style={{width:wp(39)}}>
                    <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={hp(1)} label={Strings.firstName} component={InputComponent}
                    returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>

                <View style={{marginHorizontal:wp(2), width:wp(39)}}>
                    <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={hp(1)} label={Strings.secondName} component={InputComponent} 
                        returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>
            </View>

            <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.mobileNumber} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />


            <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
               returnKeyType="done"
               onSubmit={() => {
                   Keyboard.dismiss();
               }}
            />

            <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor}   name="job" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.job} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />
            
              
            <Field borderColor='gray' editable={false} style={{ width: wp(80) }} textColor={colors.primaryColor}   name="rate" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.rate} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />
           
          
        </View>

    )
}

    render(){
        const {currentUser} = this.props;
       
        return(
            <View style={{ flex:1 }}>
                <AppHeader showBurger navigator={this.props.navigator} title={Strings.profile} /> 
                <ScrollView style={{ flex:1 }} > 

                 <TouchableOpacity
                 onPress={()=>{this.setState({ showModal: true })}}
                  style={{marginTop:hp(5), alignSelf:'center'}}>
                    <Thumbnail
                     large
                     source={this.state.userImage?{uri:this.state.userImage}:currentUser.user.img?{uri:currentUser.user.img}:this.state.userImage!=null?{uri:this.state.userImage}:require('../assets/imgs/profileicon.png')}
                    />
                 </TouchableOpacity> 

                <View style={{justifyContent:'center',alignItems:'center', flex:1, marginTop:hp(3),width:wp(100)}}>
                    <View style={{width:wp(85)}}>
                        {this.renderContent()}
                    </View>
                </View>    
                </ScrollView>
                {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}

                {/* for city and area */}
                <SnackBar
                visible={this.state.noConnection!=null?true:false} 
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

                {this.showImage()}

            </View>
            
        );
    }
}



const form = reduxForm({
    form: "UpdateProfile",
    enableReinitialize:true,
    validate,
})(UpdateProfile)


const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser : state.auth.currentUser,
    initialValues: {
        firstName: state.auth.currentUser.user.firstname,
        lastName: state.auth.currentUser.user.lastname,
        email: state.auth.currentUser.user.email,
        phoneNumber: state.auth.currentUser.user.phone,
        job: state.auth.currentUser.user.type,
        rate: ''+state.auth.currentUser.user.rate+'',
      },
    
})


export default connect(mapToStateProps)(form);

