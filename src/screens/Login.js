import React, { Component } from 'react';
import {
  View,TouchableOpacity,Keyboard,AsyncStorage,Image
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {login} from '../actions/AuthActions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);


const validate = values => {
    const errors = {};

    const phone = values.phone
    const password = values.password;

    if (phone == null) {
        errors.phone = Strings.require;
    }
    if (password == null) {
        errors.password = Strings.require;
    }

    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Login extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator;
    }
    
    componentDidMount(){    
        this.disableDrawer(); 
        console.log('anwer  '+this.props.isRTL) 
        console.log('Login page user token  '+this.props.userToken)     
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }


    onLogin(values) {
        //AsyncStorage.setItem('@password', values.password);
        this.props.login(
            values.phone,
            values.password,
            this.props.userToken,
            this.props.navigator,
        );
       
    }


    renderLoginButtons() {
        const { handleSubmit } = this.props;

        return (
            <MyButton
            onPress={handleSubmit(this.onLogin.bind(this))}
            style={{marginTop:hp(5), alignSelf:'center', justifyContent:'center',alignItems:'center',height:hp(8),width:wp(50),backgroundColor:colors.darkPrimaryColor,borderRadius:wp(10)}}
            >
                <AppText fontSize={wp(5)} text={Strings.login} color='white' />
            </MyButton>
        );
    }

    renderForgetPassword() {
        return (
            <MyTouchableOpacity
             onPress={()=>{
                this.props.navigator.push({
                    screen: 'ForgetPassword',
                    animated: true,
                });
             }}
            style={{marginTop:hp(3), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80)}}
            >
                <AppText fontSize={wp(5)} text={Strings.forgetPassword} color='black' />
            </MyTouchableOpacity>
        );
    }
    
    renderContent() {
        return (
            <View>
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="phone" isRTL={this.props.isRTL} numeric marginBottom={moderateScale(3)} label={Strings.mobileNumber} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="password" isRTL={this.props.isRTL} type="password" password={true} label={Strings.password} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />

                {this.renderLoginButtons()}
                {this.renderForgetPassword()}
            </View>

        )
    }

    render(){
        return(
            <View style={{ flex:1 }} >
               <Image style={{alignSelf:'center',marginTop:hp(15), width:wp(80),height:hp(10)}} source={require('../assets/imgs/smartTrach2.jpg')} />
               <View style={{marginTop:hp(7),width:wp(80),alignSelf:'center'}}>
                        {this.renderContent()}
                </View>
                
            {/*  <View style={{justifyContent:'center',alignItems:'center', flexDirection:this.props.isRTL?'row-reverse':'row', alignSelf:'center',position:'absolute',bottom:moderateScale(20),right:0,left:0}}>
                    <AppText fontSize={responsiveFontSize(2.5)} text={Strings.dontHaveAccount} color='gray' />
                    <MyTouchableOpacity
                    onPress={()=>{
                        this.props.navigator.resetTo({
                            screen: 'Signup',
                            animated: true,
                        });
                    }}
                    >
                        <AppText fontSize={responsiveFontSize(2.7)} text={Strings.registerNow} color={colors.buttonColor} />
                    </MyTouchableOpacity>
                </View>
            */} 
            
                {this.props.loading&&<LoadingDialogOverlay title={Strings.checkLogin} />}
                <SnackBar
                visible={this.props.errorText!=null?true:false} 
                textMessage={this.props.errorText}
                messageColor='white'
                backgroundColor={colors.darkPrimaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const form = reduxForm({
    form: "Login",
    validate,
})(Login)

const mapDispatchToProps = {
    login,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    userToken: state.auth.userToken,
   
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

