import React,{Component} from 'react';
import {View,Text,ActivityIndicator,AsyncStorage} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button,Card,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';

import MapView , { Marker } from 'react-native-maps';
import SnackBar from 'react-native-snackbar-component';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import strings from '../assets/strings';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class NotificatioDetails extends Component {

    state={
        doTaskLoading:false,
        errorText:null,
        loading:false,
        details:null,
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    constructor(props){
        super(props);
        axios.get(`${BASE_END_POINT}trash/${this.props.subject}`)
        .then(response => {
            console.log("noti Details Done");
            console.log(response.data);
            this.setState({details:response.data})
        }).catch(error => {
            console.log("noti Details Error")
            console.log(error.response)
            console.log("noti Details Error")

        })
    }

    iDoTask =async () => {
        const flag = await AsyncStorage.getItem('@TrachID');
        if(flag){
            RNToasty.Warn({title:strings.taskIsStall})
        }else{
            this.setState({doTaskLoading:true})
            axios.put(`${BASE_END_POINT}trash/${this.props.subject}/onprogress`, {}, {
                headers: { 
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`    
                } 
        })
        .then(response=>{
            console.log(response);
            console.log('details done')
            this.setState({doTaskLoading:false})
            if(response.data.toLowerCase().includes('someone have this task'.toLocaleLowerCase())){
                RNToasty.Info({title:strings.someneHaveTask})
            }else{
                RNToasty.Success({title:strings.youHaveTask})
                AsyncStorage.setItem('@TrachID',this.props.subject.toString())
            }
                            
        }).catch(error=>{
            console.log('details error')
            console.log(error);
            console.log(error.response);
            this.setState({doTaskLoading:false})

        }) 
        
        }
    }
  
    render(){
        const {navigator,description,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.taskDetails} showBack navigator={navigator}/>
               <View style={{height:'45%'}}>
               <View style={{width:wp(70),marginVertical:hp(5),justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
                    <Thumbnail large source={require("../assets/imgs/sg2.png")} />
                    <Text style={{fontSize:wp(5),marginVertical:hp(2)}}>{description}</Text>
                </View>

                <View style={{marginVertical:hp(0), height:hp(6), alignSelf:'center', borderBottomWidth:0.5,borderBottomColor:'gray', width:wp(100),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{color:colors.darkPrimaryColor, marginHorizontal:wp(5), fontSize:wp(5)}}>{strings.color}</Text>
                    {
                        this.state.details?
                        <Text style={{marginHorizontal:wp(5), fontSize:wp(5)}}>{this.state.details.color}</Text>
                        :
                        <ActivityIndicator size='small' />
                    }
                </View>

                <View style={{marginTop:hp(2), height:hp(6), alignSelf:'center', width:wp(100),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{color:colors.darkPrimaryColor, marginHorizontal:wp(5), fontSize:wp(5)}}>{strings.number}</Text>
                    {
                        this.state.details?
                        <Text style={{marginHorizontal:wp(5), fontSize:wp(5)}}>{this.state.details.number}</Text>
                        :
                        <ActivityIndicator size='small' />
                    }
                </View>

                </View>
              

                <View style={{width:wp(100),height:'40%'}}>
                <MapView
                    style={{width:wp(100),height:'100%'}}
                    region={{
                    
                        latitude:this.state.details?this.state.details.destination[0] : 37.78825,
                        longitude:this.state.details?this.state.details.destination[1] : -122.4324,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    
                    }}
                    >
                     <Marker
                        coordinate={{
                            latitude:this.state.details?this.state.details.destination[0] : 37.78825,
                            longitude:this.state.details?this.state.details.destination[1] : -122.4324}}
                        //title={marker.title}
                        //description={marker.description}
                     />
                </MapView>
                </View>
               
               
               <View style={{elevation:2, position:'absolute',bottom:0,right:0,left:0}}>
               <Button onPress={()=>this.iDoTask()} style={{ width:wp(100), backgroundColor:colors.buttonColor,justifyContent:'center',alignItems:'center',}} >
                   <Text style={{color:'white',fontSize:wp(5)}}>{strings.iDoTheTask}</Text>
               </Button>
               </View>
                {this.state.doTaskLoading&&<LoadingDialogOverlay title={strings.wait} />}
                <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={this.state.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}



const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
})

export default connect(mapStateToProps)(NotificatioDetails);
