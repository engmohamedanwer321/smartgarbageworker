import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import MenuReducer from "./MenuReducer";
import NavigationReducer from './NavigationReducer';
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import NotificationsReducer from './NotificationsReducer';


export default combineReducers({
    form: formReducer,
    menu: MenuReducer,
    navigation: NavigationReducer,
    auth: AuthReducer,
    lang:LanguageReducer,
    noti: NotificationsReducer,
    
});