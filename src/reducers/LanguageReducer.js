import * as types from "../actions/types"

const initialState = {
    RTL: false,
}

const LanguageReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHANGE_LANGUAGE:
            console.log('in reducer =>'+action.payload)
            return { ...state, RTL: action.payload };    
        default:
            return state;
    }

}

export default LanguageReducer;